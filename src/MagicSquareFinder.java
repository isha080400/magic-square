import java.util.Scanner;

public class MagicSquareFinder {

    public static void isMagicSquare() {
        Scanner sc = new Scanner(System.in);

        int sizeOfMatrix = sc.nextInt();
        int[][] matrix = new int[sizeOfMatrix][sizeOfMatrix];

        for (int i = 0; i < sizeOfMatrix; i++) {
            for (int j = 0; j < sizeOfMatrix; j++) {
                matrix[i][j] = sc.nextInt();
            }
        }

        int[] sumOfRows = new int[sizeOfMatrix];
        int[] sumOfColumns = new int[sizeOfMatrix];

        for(int i = 0; i < sizeOfMatrix; i++){
            for(int j = 0; j < sizeOfMatrix; j++){
                sumOfRows[i] += matrix[i][j];
                sumOfColumns[i] += matrix[j][i];
            }
        }

        int i = 0, j = 0, flag = 0;
        int[] sumOfDiagonal = new int[2];

        while(i < sizeOfMatrix && j < sizeOfMatrix){
            sumOfDiagonal[0] += matrix[i][j];
            i++;
            j++;
        }

        i = 0;
        j = (sizeOfMatrix - 1);

        while(i < sizeOfMatrix && j >= 0){
            sumOfDiagonal[1] += matrix[i][j];
            i++;
            j--;
        }

        for(i = 1; i < sizeOfMatrix; i++){
            if(sumOfRows[i] != sumOfRows[i - 1]){
                flag = 1;
                break;
            }
        }


        if(flag != 1) {
            for (i = 1; i < sizeOfMatrix; i++) {
                if(sumOfColumns[i] != sumOfColumns[i - 1]){
                    flag = 1;
                    break;
                }
            }
        }

        if(flag != 1){
            if(sumOfDiagonal[0] != sumOfDiagonal[1]){
                flag = 1;
            }
        }

        if(flag == 1){
            System.out.println("no");
        }
        else{
            System.out.println("yes");
        }

    }
}
